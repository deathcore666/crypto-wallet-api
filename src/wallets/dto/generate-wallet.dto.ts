import { IsString } from 'class-validator';

export class GenerateWalletDto {
  @IsString()
  name: string;
}
