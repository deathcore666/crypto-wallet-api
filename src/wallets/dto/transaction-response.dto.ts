export class TransactionResponseDto {
  data: transactionData[];
  success: string;
  meta: {
    at: number;
    page_size: number;
  };
}

class transactionData {
  transaction_id: string;
  token_info: tokenInfo;
  block_timestamp: number;
  from: string;
  to: string;
  type: string;
  value: number;
}

class tokenInfo {
  symbol: string;
  address: string;
  decimals: number;
  name: string;
}