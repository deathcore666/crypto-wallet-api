import { IsNumber, IsString } from 'class-validator';

export class TransferFundsDto {
  @IsString()
  toAddress: string;

  @IsNumber()
  amount: number;
}
