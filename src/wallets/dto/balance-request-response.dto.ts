export class BalanceRequestResponseDto {
  balance: number;
  address: string;
  balancePretty: string;
}
