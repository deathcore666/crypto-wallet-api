import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { WalletsService } from './wallets.service';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { Wallet } from './schemas/wallet.schema';
import { TransferFundsDto } from './dto/transfer-funds.dto';

@Controller('wallets')
export class WalletsController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor(
    private walletsService: WalletsService,
    @InjectConnection() private connection: Connection,
  ) {}

  @Get()
  async findAll(): Promise<Wallet[]> {
    return this.walletsService.findAll();
  }

  @Get('/balance')
  async getWalletBalanceByAddress() {
    return this.walletsService.getMainWalletBalance();
  }

  @Get('/balance/:id')
  async getWalletBalanceById(@Param('id') id: string) {
    return this.walletsService.getBalanceByWalletId(id);
  }

  @Post('/transfer')
  async transfer(@Body() transferFundsDto: TransferFundsDto) {
    return this.walletsService.transferFunds(transferFundsDto);
  }

  @Get('/transactions/')
  async getTransactionsByWalletAddress() {
    return this.walletsService.getTransactions();
  }

  @Get('/transactions/:hex')
  async getTransactionsByWalletAddressHex(@Param('hex') hex: string) {
    return this.walletsService.getTransactionsByWalletAddressHex(hex);
  }
}
