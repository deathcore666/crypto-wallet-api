import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type WalletDocument = Wallet & Document;

@Schema()
export class Wallet {
  @Prop()
  name: string;

  @Prop()
  privateKey: string;

  @Prop()
  publicKey: string;

  @Prop(
    raw({
      base58: { type: String },
      hex: { type: String },
    }),
  )
  address: Record<string, any>;
}

export const WalletSchema = SchemaFactory.createForClass(Wallet);