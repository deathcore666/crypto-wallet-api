import { Model } from 'mongoose';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as TronWeb from 'tronweb';
import * as TronGrid from 'trongrid';
import { HttpService } from '@nestjs/axios';
import { map, Observable, timestamp } from 'rxjs';
import { AxiosResponse } from 'axios';

import { Wallet, WalletDocument } from './schemas/wallet.schema';
import { GenerateWalletDto } from './dto/generate-wallet.dto';
import { TransferFundsDto } from './dto/transfer-funds.dto';
import { BalanceRequestResponseDto } from './dto/balance-request-response.dto';

import { TransactionResponseDto } from './dto/transaction-response.dto';

@Injectable()
export class WalletsService {
  constructor(
    @InjectModel(Wallet.name) private walletModel: Model<WalletDocument>,
    private httpService: HttpService,
  ) {
  }

  public tronWeb = new TronWeb({
    fullHost: process.env.TRON_NETWORK_URL,
    headers: { 'TRON-PRO-API-KEY': process.env.TRON_PRO_API_KEY || '' }, // eslint-disable-line
    privateKey: process.env.TRON_MAIN_WALLET_PRIVATE_KEY,
  });

  public tronGrid = new TronGrid(this.tronWeb);

  //todo
  create(generateWalletDto: GenerateWalletDto): Promise<Wallet> {
    const wallet: Wallet = null;
    // const generatedWallet =

    return new Promise<Wallet>((res) => {
      res(wallet);
    });
  }

  findAll(): Promise<Wallet[]> {
    return this.walletModel.find().exec();
  }

  getTransactionsByWalletAddressHex(hex: string) {


  }

  async getTransactions(): Promise<
    Observable<AxiosResponse<TransactionResponseDto>>
  > {
    if (!this.tronWeb.isAddress(process.env.TRON_MAIN_WALLET_ADDRESS)) {
      throw new BadRequestException(null, 'Invalid Wallet Address');
    }

    return this.httpService
      .get(
        `https://api.trongrid.io/v1/accounts/${process.env.TRON_MAIN_WALLET_ADDRESS}` +
          `/transactions/trc20?limit=20&contract_address=${process.env.TRON_USDT_CONTRACT}`,
      )
      .pipe(map((response) => response.data));

    // this.tronWeb.setDefaultBlock('latest');
    // const result = await this.tronGrid.contract.getEvents(
    //   process.env.TRON_USDT_CONTRACT,
    //   {
    //     only_confirmed: false,
    //     event_name: 'Transfer',
    //     limit: 200,
    //     order_by: 'timestamp, desc',
    //     min_timestamp: Number(timestamp()),
    //   },
    // );
    // result.data = result.data
    //   .map((tx) => {
    //     tx.result.to_address = this.tronWeb.address.fromHex(
    //       tx.result.to_address,
    //     ); // this makes it easy for me to check the address at the other end
    //     return tx;
    //   })
    //   .filter(
    //     (tx) => tx.result.to_address === process.env.TRON_MAIN_WALLET_ADDRESS,
    //   );
    //
    // return result;
  }

  async getContract() {
    const { abi } = await this.tronWeb.trx.getContract(
      process.env.TRON_USDT_CONTRACT,
    );

    return this.tronWeb.contract(abi.entrys, process.env.TRON_USDT_CONTRACT);
  }

  async getMainWalletBalance(): Promise<BalanceRequestResponseDto> {
    if (!this.tronWeb.isAddress(process.env.TRON_MAIN_WALLET_ADDRESS)) {
      throw new BadRequestException(null, 'Invalid Wallet Address');
    }

    const contract = await this.getContract();

    // const balance = await this.tronWeb
    //   .contract()
    //   .at(process.env.TRON_USDT_CONTRACT);

    const balance = await contract.methods
      .balanceOf(process.env.TRON_MAIN_WALLET_ADDRESS)
      .call();

    const decimals = await contract.methods.decimals().call();

    return {
      balance: Number(balance),
      balancePretty: Number(balance) / Math.pow(10, decimals) + ' USDT',
      address: process.env.TRON_MAIN_WALLET_ADDRESS,
    };
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  getBalanceByWalletId(id: string) {}

  async transferFunds(transferFundsDto: TransferFundsDto): Promise<any> {
    if (!this.tronWeb.isAddress(transferFundsDto.toAddress)) {
      throw new BadRequestException(null, 'Invalid Wallet Address');
    }

    const balance = await this.getMainWalletBalance();
    if (transferFundsDto.amount > balance.balance) {
      throw new BadRequestException(null, 'Not enough funds');
    }
    const contract = await this.getContract();
    return await contract.methods
      .transfer(transferFundsDto.toAddress, transferFundsDto.amount)
      .send();
  }

  async getMainWalletTrxBalance() {
    if (!this.tronWeb.isAddress(process.env.TRON_MAIN_WALLET_ADDRESS)) {
      throw new BadRequestException(null, 'Invalid Wallet Address');
    }
    return this.tronWeb.trx.getAccountResources(process.env.TRON_MAIN_WALLET_ADDRESS);
  }
}
