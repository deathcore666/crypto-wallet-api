import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WalletsModule } from './wallets/wallets.module';
import { TransactionsModule } from './transactions/transactions.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';''
@Module({
  imports: [
    ConfigModule.forRoot(),
    WalletsModule,
    TransactionsModule,
    MongooseModule.forRoot('mongodb://localhost:27017/crypto-wallet-app'),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
