const TronWeb = require('tronweb');
const HttpProvider = TronWeb.providers.HttpProvider;
const fullNode = new HttpProvider('https://api.shasta.trongrid.io');
const solidityNode = new HttpProvider('https://api.shasta.trongrid.io');
const eventServer = new HttpProvider('https://api.shasta.trongrid.io');
const privateKey = 'f9afb7dcda665c40429b6ce3c0be45969ed79658a9c27c47418a3ae0f76f99f7';
const ACCOUNT = 'TPH1qjF91Xek7zv7bWsS83FvZKStm7q9Up';
const TOACCOUNT = 'TSh1LweFJ39QDYWhuivbnnkVZKtDoX7hrG';

const CONTRACT = 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t'; // USDT contract
const tronWeb = new TronWeb(fullNode, solidityNode, eventServer, privateKey);

async function main() {
  // const userBalance = await tronWeb.trx.getBalance(ACCOUNT);
  // console.log(`User's balance is: ${ userBalance }`);

  const {
    abi
  } = await tronWeb.trx.getContract(CONTRACT);

  console.log(JSON.stringify(abi));

  const contract = tronWeb.contract(abi.entrys, CONTRACT);

  const balance = await contract.methods.balanceOf(ACCOUNT).call();
  console.log("balance:", balance.toString());

  // const resp = await contract.methods.transfer(ACCOUNT, 1000).send();
  // console.log("transfer:", resp);
}

main()
  .then(() => {
    console.log('ok');
  })
  .catch((err) => {
    console.log('error:', err);
  });
